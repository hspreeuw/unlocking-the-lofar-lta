\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\graphicspath{{images/}}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}

\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{url}

% For sub figures
% \usepackage{subcaption}

% For interactive sharing
%\usepackage{soul}
%\newcommand{\hlc}[2][yellow]{ {\sethlcolor{#1} \hl{#2}} }
%\newcommand{\sm}[1]{\hlc{#1}}
%\newcommand{\smb}[1]{\hlc[green]{#1}}

\newcommand{\etal}{\textit{et al.}\xspace}
\newcommand{\idest}{\textit{i.e.}\xspace}
\newcommand{\eg}{\textit{e.g.}\xspace}
\newcommand{\aka}{\textit{a.k.a.}\xspace}
\newcommand{\tit}[1]{\textit{#1}}
\newcommand{\thi}[1]{\texttt{#1}}
\newcommand{\gra}[1]{\textbf{#1}}

% Reduce space between figures and text (not caption}
%\setlength\belowcaptionskip{-3ex}
%\setlength{\parskip}{0mm}
\setlength{\textfloatsep}{5pt}

\begin{document}

\title{Unlocking the LOFAR LTA\\
%{\footnotesize \textsuperscript{*}Note: Sub-titles are not captured in Xplore and
%should not be used}
\thanks{This project has received funding from the European Union’s Horizon 2020 research and innovation programme under Grant Agreement 777533.}
}

\author{\IEEEauthorblockN{1\textsuperscript{st} Hanno Spreeuw}
\IEEEauthorblockA{\textit{} 
\textit{The Netherlands eScience Center}\\
Amsterdam, The Netherlands \\
h.spreeuw@esciencecenter.nl}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Souley Madougou}
\IEEEauthorblockA{\textit{}
\textit{The Netherlands eScience Center}\\
Amsterdam, The Netherlands \\
s.madougou@esciencecenter.nl}
\and
\IEEEauthorblockN{3\textsuperscript{th} Ronald van Haren}
\IEEEauthorblockA{\textit{} 
\textit{The Netherlands eScience Center}\\
Amsterdam, The Netherlands \\
r.vanharen@esciencecenter.nl}
\and
\IEEEauthorblockN{4\textsuperscript{rd} Berend Weel}
\IEEEauthorblockA{\textit{}
\textit{The Netherlands eScience Center}\\
Amsterdam, The Netherlands \\
b.weel@esciencecenter.nl}
\and
\IEEEauthorblockN{5\textsuperscript{th} Adam Belloum}
\IEEEauthorblockA{\textit{}
\textit{The University of Amsterdam}\\
Amsterdam, The Netherlands \\
a.s.z.belloum@uva.nl}
\and
\IEEEauthorblockN{6\textsuperscript{th} Jason Maassen}
\IEEEauthorblockA{\textit{}
\textit{The Netherlands eScience Center}\\
Amsterdam, The Netherlands \\
j.maassen@esciencecenter.nl}

%\and
%\IEEEauthorblockN{6\textsuperscript{th} Given Name Surname}
%\IEEEauthorblockA{\textit{dept. name of organization (of Aff.)} \\
%\textit{name of organization (of Aff.)}\\
%City, Country \\
%email address}
}

\maketitle

\begin{abstract} 
%Scientific progress is, to a large extent, based on measurements from increasingly sensitive instruments which tend to produce amounts of data that require some form of compression for 
%long term storing. Archives from such modern instruments contain complex data in formats that are not easily understood by scientists away from the immediate vicinity of the instrument. In turning such 
%archives into valuable resources for a wider community efforts are needed, but these can increase the scientific output of the instrument considerably.

In this paper we discuss our efforts in ``unlocking'' the Long Term Archive (LTA) of the LOFAR radio telescope. This is a large ($>$ 43 PB) archive that expands with about 7~PB per year by the ingestion of 
new observations. It consists of coarsely calibrated "visibilities", i.e. correlations between signals from LOFAR stations. Currently, only a small fraction of the LOFAR LTA consists of sky maps, which 
are needed for most astronomical research. Unfortunately, creating such sky maps can be challenging, due to the data sizes of the observations and the complexity and compute requirements of the software 
involved. We try to fix this by enabling a simple one-click-reduction of LOFAR observations into sky maps for any user of this archive. This work was performed as part of the 
PROCESS\footnote{\url{https://process-project.eu}} project, which aims to provide generalizable open source solutions for user friendly exascale data processing. 
\end{abstract}

\begin{IEEEkeywords}
data dissemination, data aggregation, data mining, data processing
%Data processing, Radio astronomy, User interfaces, HPC, API, Web services, Extreme large scale
\end{IEEEkeywords}

\section{Introduction}
The LOw Frequency Array (LOFAR), which covers frequencies between 10 and 250 MHz, is a European radio telescope with 51 stations that became operational in 2010. Its design differs from classical radio telescopes that consist of arrays of dishes. Instead, LOFAR combines the signals from 96 Low Band Antennas or $2 \times 24$ High Band Antennas (HBA field is split) into one station signal, similar to the signal of a single dish from a classical radio telescope. For imaging observations the station signals are correlated per baseline - a pair of two stations - following the principles of aperture synthesis; i.e. every pair of signals is multiplied and integrated over the time sampling interval. This means that a LOFAR imaging observation results in a large set of such correlations - generally termed visibilities. A visibility is recorded as a complex number for each baseline, frequency and time sampling interval and polarization product.

The LOFAR LTA \cite{LOFARLTA} was set up to store all LOFAR observations. A typical LOFAR observation typically takes 8-12 hours and has a size of about 100 TB. Frequency averaging of every eight channels reduces this size to about 16 TB. Initial (coarse) calibration is also applied. The last few years the LTA has been expanding by about 7 PB per year and now (2019) it is exceeding 43 PB. These coarsely calibrated and frequency averaged observations are sets of visibilities that are generally not suitable as a starting point for scientific research. In astronomy, one generally embarks on a scientific investigation by inspecting sky maps. The motive may be, for instance,  the detection of an explosion by a gamma-ray satellite at a particular position on the sky. Understanding the nature of such an event often requires multi-wavelength analysis. Consequently, astronomers will want to find out what kind of object images from observations at other wavelengths, like radio, show at this position on the sky. This is where a repository with low frequency radio maps covering a large part of the sky can provide a useful resource. Unfortunately, sky maps are available only for a small fraction of the LOFAR observations. This can be accommodated for by offering "one-click-processing" of observations: after selecting an observation, an appropriate reduction pipeline and a set of reduction parameters, acquiring a well calibrated sky map just requires waiting for staging - copying from magnetic tape to disk - and for processing to complete.

%The LOFAR telescope is usually dubbed a ``software'' telescope, meaning it relies on software to reach its goals. For instance, software is used to introduce delays between the station signals, thereby "steering" the field of view, while all antennas, of course, remain fixed on the ground. For a classical telescope of dishes, on the other hand, a source on the sky is tracked by mechanically adjusting the pointings of the dishes.
% This is visible in the operational design of the instrument as, before even being stored as data, the signals go through a computing system, the correlator, where they are processed (correlated) and then sent to the LTA as visibilities. 
As mentioned above, the complex visibilities from the correlator are seldom used as a starting point for astronomical research. Instead, these data need to go through further processing such as radio frequency interference removal, calibration and finally imaging. These images can then be used as the beginnings of astronomical research. Each part of the above processing is complex and usually requires both domain and software knowledge in order to generate useful output. This, combined with the massive volumes of the data, makes the use of the LTA only possible for a select group of people. Furthermore, these challenges are exacerbated when one needs to process many observations. For this reason, researchers interested in surveys, for instance, are building frameworks to make LOFAR observation processing easy, portable, scalable and generalisable\cite{mechev2017}.

A large part of this research was conducted from one the five use cases for PROCESS\footnote{\url{https://www.process-project.eu/projects/square-kilometre-array-lofar-ska/}}. One of the goals of PROCESS is to facilitate the use of the resulting platform by providing a user-friendly frontend that lets the user select both her dataset and workflow, launch the processing to the computing infrastructures and, finally, either directly or indirectly, retrieve the results from the frontend. For the particular case of LOFAR data processing, such a tool already exists, although it needs to be customised and extended; as part of the EU EOSC Pilot for LOFAR project\footnote{\url{https://eoscpilot.eu/lofar-data}}, an astronomer-friendly frontend to LOFAR LTA has been built.

A second goal of PROCESS is to offer exascale computing services to a range of scientists that require big storage and big compute facilities, in such a way that these users can remain completely agnostic of the location and specifications of the compute clusters where their data will be processed. In Europe, we currently do not have any exaflop supercomputer. This means that any form of processing requiring exascale computing will have to be distributed over a number of clusters in Europe. Such distributions over many clusters will have to be performed seamlessly and all software packages that run the computations will have to be containerized to guarantee portability. So, in summary, scalability is a main requirement for the PROCESS software infrastructure, not only with respect to compute power, but also with respect to data transfer.

One of the achievements of this project is to make workflow processing possible on a few clusters connected to the LTA - such as the clusters hosted by SURFsara\footnote{\url{https://userinfo.surfsara.nl/}} - with high-bandwidth network  and accelerating some of the processing steps by using parallel computing. This can be seen as vertical scalability. One of the goals the project did not reach yet is horizontal scalability. Indeed, PROCESS is designed to tackle the extreme large data handling and processing challenges introduced by exascale applications such as astronomical surveys. The approach taken by PROCESS is to build tools and services capable of supporting exascale applications by federating computing infrastructures from both HPC and Cloud. 
%It comes at no surprise, LOFAR surveys is one of five pilot applications in PROCESS which will bring horizontal scalability to above work by making it possible to run several processing workflows in parallel on several computing infrastructures.

%\section{Background}
%\subsection{LOFAR software stack}
%\subsection{EOSC pilot for LOFAR}
%\subsection{Xenon-flow and CWL}
%\subsection{Containerisation and data services in PROCESS}

%The use of the LOFAR telescope by the different Key Science Projects (KSP) is based on the concept of pipelines. A pipeline is a chain structured as a directed acyclic graph (DAG) of processing steps, each step taking some inputs, performing some processing and generating some outputs. This pipeline framework is used to automate processing on computing facilities (at ASTRON). For instance, almost every KSP has its own pipeline.
% This concept is deeply entranched into the way the telescope data are used.
%Writing a pipeline is quite complex and requires knowledge of the pipeline framework and programming skills. Because of this, a specific pipeline, called \thi{genericpipeline}, is offered for helping users design and execute their workflows without understanding the underlying pipeline system. Usually, there are enough predefined steps for the user to choose from, so that defining a pipeline with the \thi{genericpipeline} boils down to configuring a minimal set of program arguments. Steps and arguments are defined in a so-called parameter set (\thi{parset}) file which is the argument to the \thi{genericpipeline}.
%
%Most astronomers use images as starting point for their analysis. The generation of those images goes through several steps, including pre-processing, calibration and actual imaging. One common tool used in this process is \thi{prefactor} which consists of various \tit{parsets} for the \thi{genericpipeline} that do the direction-independent (DI) calibration of LOFAR data and various \thi{Python} plugins as specialised pipeline steps. Originally intended to prepare the data for use by the direction-dependent (DD) calibration software \thi{factor} (thus, the name), \thi{prefactor} corrects for various instrumental and ionospheric effects on observations and makes them ready for other DD calibration software, such as \thi{killMS}, as well. It is essentially composed of two pipelines: the \tit{calibrator} pipeline processing the calibrator to derive the DI corrections and the \tit{target} pipeline transferring the DI corrections to the target and performing DI calibration on the target.

%As part of the EOSC Pilot for LOFAR, work has been done to make LOFAR data available to a broader audience for enabling groundbreaking scientific discoveries. To reach this goal, LOFAR data will become more compliant with FAIR principles and containerisation is used to make processing portable across EOSC infractures. Containerisation is a new form of delivering software preinstalled in an isolated environment, bundled with all its dependencies. This makes software installation easy, as the user only needs the actual container once the container software is in place. In the EOSC Pilot for LOFAR, the common workflow language (CWL\footnote{\url{https://www.commonwl.org/v1.0/}}) is used to ensure data analysis workflow portability across systems. Concretely, the output of the EOSC Pilot for LOFAR is an astronomer-friendly web application where one can select an observation from LTA and a pipeline from the frontend, then launch the processing to the backend supported by EOSC infrastructures using CWL and Singularity\footnote{\url{https://www.sylabs.io}}. Unfortunately, the remote execution framework used in that project (PiCaS) is not easily portable. Consequently, we replace it by a more portable solution. Yet, we use the frontend for data selection.

%For the remote execution framework used in this work is the \thi{xenon-flow}\footnote{\url{https://github.com/xenon-middleware/xenon-flow}} middleware which runs CWL workflows using \thi{xenon}\cite{jason_maassen_2015_35415}. Xenon is a middleware abstraction library providing a simple programming interface to many remote compute and storage resources and hiding the complex protocol and tool specific details from the application. This allows the application to switch between resources without changing the code. Xenon is based on a few fundamental concepts, including credentials, file system, scheduler and job. Credentials contain functionality for gaining access to resources, file systems for file management and schedulers create and manage jobs usually though a workload management system.

\section{Implementation and Results}
\subsection{Web Interface}
The Web application (LTACAT) is a React application based on \thi{FRBCAT}\footnote{\url{http://www.frbcat.org}} developed in the AA-ALERT\footnote{\url{https://www.esciencecenter.nl/project/aa-alert}} project. This application is customised and extended to fit LOFAR LTA needs. In the frontend, the main database view (Fig.~\ref{ltacatdbview}) remains mostly unchanged, but showing information about LOFAR observations, instead of fast radio bursts (FRBs). The frontend is however extended with a pipeline configurator and launcher. The backend is rewritten to use the LTA database view, which lets the application seamlessly access the observational archived data. Next, the backend is further extended with a RESTful \cite{fielding_rest} backend service developed using Django. The purpose of this service is to provide a window to the Web application to integrate and execute pipelines, i.e. it provides a list of available pipelines, provides configuration parameters for selected pipelines, and allows for executing a user configured pipeline for a selected observation. The pipeline configurator (Fig.~\ref{pipelineconf}) in the Web application is automatically rendered from the \thi{JSON schema}\footnote{\url{https://json-schema.org/}} that is provided by the backend service. \thi{JSON schema} was chosen as the format for providing the pipeline configuration as it allows for, among others,  setting default values and constraints on inputs, setting required properties, and defining dependencies between properties.

\begin{figure}[tb]
\centerline{\includegraphics[width=.5\textwidth]{ltacat}}
\caption{Main database view.}
\label{ltacatdbview}
\end{figure}

\begin{figure}[tb]
\centerline{\includegraphics[width=.5\textwidth]{prefactor}}
\caption{Prefactor listed as an available pipeline.}
\label{pfp}
\end{figure}

By default the backend service comes bundled with a pre-defined pipeline, but is easily extended. It allows users to develop additional pipelines and integrate them in the Web application. For this purpose a pipeline template is provided in addition to a step-by-step guide on how to integrate new pipelines into the service. The procedure consists of implementing a \tit{run} function, defining the pipeline configuration parameters in \thi{JSON schema} format, and registering the pipeline in the pipeline administrator of the service.  After installation of the new pipeline based on these steps, a new pipeline appears in the list of available pipelines next to the default one as illustrated in the image in Fig.~\ref{pfp}. Here we added the \thi{prefactor}\footnote{\url{https://www.astron.nl/citt/prefactor/}} pipeline.

\begin{figure}[tb]
\centerline{\includegraphics[width=.5\textwidth]{xenon-flow}}
\caption{Pipeline configurator.}
\label{pipelineconf}
\end{figure}

\subsection{DDF pipeline}
\thi{prefactor} takes care of the first step in calibration, which is direction independent. For science quality images, this has to be followed by direction dependent calibration. This is caused by the LOFAR station beam that covers a patch of the sky so wide that ionospheric phase delays differ within that patch. In order to take these effects into account the field of view is often split in hundreds of directions and calibration solutions are derived for each of these directions. A few calibration packages are available that solve for multiple directions on the sky, like \thi{factor}\footnote{\url{https://github.com/lofar-astron/factor}}, \thi{SAGECal}\footnote{\url{https://github.com/nlesc-dirac/sagecal}}, \thi{BBS}\footnote{\url{https://support.astron.nl/LOFARImagingCookbook/bbs.html}} and \thi{DPPP}\footnote{\url{https://support.astron.nl/LOFARImagingCookbook/dppp.html}}, but we chose \thi{killMS}\footnote{\url{https://github.com/saopicc/killMS}} because it is part of a complete imaging pipeline - the \thi{DDF-pipeline}\footnote{\url{https://github.com/mhardcastle/ddf-pipeline}} - that has proven its robustness in the the reduction of petabytes of LOFAR data as part of the \thi{LOFAR Two-metre Sky Survey}\cite{shimwell2017lofar}. The core part of the \thi{DDF-pipeline} is a \tit{self-calibration} loop: initial calibration solutions are used to produce a first map of the sky. The sources in that map are subsequently used to derive better calibration solutions, which are used to produce a more accurate map, and so forth, see \cite{tasse2018faceting}.

\subsection{CWL \& Xenon}
To run the pipeline in a scalable manner we chose to adopt the Common Workflow Language (CWL\footnote{\url{https://www.commonwl.org/v1.0/}}) to describe the pipeline and use \thi{xenon-flow}\footnote{\url{https://github.com/xenon-middleware/xenon-flow}} to run it on compute infrastructure. Xenon-flow is a RESTful Web service that executes CWL workflows on compute infrastructure using the \thi{Xenon}\cite{jason_maassen_2015_35415} middleware and a CWL runner. It runs as a server in the background and waits for requests from clients.

As Xenon-flow runs CWL workflows, a CWL file describing the pipeline has been created. The implemented pipeline currently consists of three main components: a \tit{calibrator}\footnote{Radio source with known properties.} calibration, the target calibration and the imaging of the direction independent calibrated data. Each component is described in its own CWL file as a workflow step and wrapped in a container to ensure its cross platform compatibility. As the steps are dependent on each others output we parse the standard output of each of these steps for specific string values and have xenon-flow return the concatenation of these as final output status for the workflow. This leads to three more steps in the CWL workflow, each parsing the standard output of the previous main steps, the full DAG is shown in Fig.~\ref{cwl}.

\begin{figure}[tb]
    \centerline{\includegraphics[trim=0 0 0 1cm clip=true,height=.3\textwidth]{cwlgraph}}
    \caption{Prefactor CWL workflow.}
    \label{cwl}
\end{figure}


After clicking the ``Submit Workflow’’ button, a HTTP POST request containing all required information for the pipeline is sent to xenon-flow server. Upon reception of the request, xenon-flow will start running the workflow. The configuration of xenon-flow will determine where and how the processes corresponding to the steps will be executed. In our case, xenon-flow is configured to run jobs on the Netherlands ASCI DAS5\footnote{\url{https://www.cs.vu.nl/das5/}} cluster located at the VU university using the CWL reference implementation \tit{cwl-runner}. Since we are targeting an HPC compute cluster we use \thi{Singularity} \cite{kurtzer_singularity} as the container platform.

Upon completion, the concatenated string output is available in xenon-flow and is parsed. Following successful completion, FITS images produced by the imaging step are available on DAS5, from where they are retrieved using the Python \thi{Fabric} module directly into the Django application directory structure. Once on the local machine, these FITS images are converted into JPEG images for visualisation in the browser using the Python \thi{APLpy} module.

An example of images obtained through the developed pipeline is shown in Fig.~\ref{res}.

\begin{figure}[tb]
    \centerline{\includegraphics[width=.5\textwidth]{results}}
    \caption{Images of the patch of the sky covered by the processed data. On the left, the uncalibrated data and on the right the DI-calibrated data. DD calibration will further improve the quality of the image.}
    \label{res}
\end{figure}

\subsection{Data Services}

The data infrastructure needed to unlock the LOFAR LTA has to be scalable, reliable, and easy to be installed and integrated which commonly uses data stack solutions by the scientific research community.  The approach proposed to manage the data of the LOFAR LTA that can handle the multitude of different data models, applications, distribution and management is through virtualization, by encompassing all these requirements in a data micro-infrastructure with specific nodes for handling the different aspects e.g. data sharing (nextCloud) remote data access (GridFTP), 
and HDFS file system for computing, etc. The whole infrastructure then becomes an ensemble of micro-infrastructures each with its own full stack encapsulated in a virtual infrastructure. Through templating, micro-infrastructures can be booted up that will satisfy the groups’ requirements for data processing. Cross provider data, process distribution and management are handled from within the micro-infrastructure. Cross group collaboration is also easily manageable e.g. a group could give access to another group through their data sharing node inside the micro-infrastructure. This decomposition allows for better scalability since state management such as indices, is split between many infrastructures. For this reason, we leverage the power of containers and created a platform using Kubernetes\footnote{\url{https://kubernetes.io}} where users create and infrastructure with their own dedicated data services. Typical data services include data store adaptors to connect to remote data such as HPC file systems, native cloud storage using Ceph\footnote{\url{https://ceph.com}} block storage, runtime services that have access to storage such as WebDAV points, Jupyter notebooks and data staging services. 

%The LOFAR LTA pipelines are thus executed by containers and when, say, two LOFAR archival observations are processed simultaneously, this can be enabled by doubling the number of containers - assuming these observations are of the same size. Simultaneous or quasi-simultaneous processing of multiple observations has the benefit of reducing latencies induced by data transfers - i.e. staging of observational data, from tape to dCache and from dCache to a compute cluster - and by compute bottlenecks. Data transfers may take significant time due to the data sizes and distances involved. Even at 10 GBit/s, a 16 TB dataset will require about 4 hours to transfer.  Fortunately, copying data from temporary disk to the processing location may be done per observational subband. Thus, staging and copying can overlap. Compute bottlenecks can occur in between the two subsequent calibration steps. The first step is direction independent and is embarrassingly parallel, by distributing the different subbands of a single observation (typically 244) over the different nodes, with one subband per node. Processing can start as soon as a subband has been copied to a node disk. This takes typically four hours, but the next step is direction dependent calibration and its algorithm needs a unified memory space to compute the calibration solutions. This typically takes four days on a single fat node with hundreds of GB of RAM, which would render the remaining nodes idle when processing a single observation. Processing of multiple LOFAR archival observations simultaneously by many containers will reduce latencies on the compute nodes after the first calibration step of the first observation has been completed. Also, it is important that reservation of the compute nodes is done in an intelligent manner, i.e. that the nodes will not be waiting for data to arrive at the cluster. 
%PROCESS can offer access to the three sites where the LOFAR Long Term Archive is stored - Amsterdam, Jülich and Poznan, making simultaneous combined staging and processing of observations possible.

\section{Conclusion and future work} 

In this paper we described our ongoing efforts to develop a framework that enables simple one-click-reduction of LOFAR observations into sky maps. Ease of use and scalability are important requirements 
in the development of this framework. Our goal is to significantly reduce the time and effort required to generate skymaps from the selected observations in the archive.
Currently, the framework allows for the user to select a LOFAR observation and a scientific workflow, launch the processing onto a (single) computing infrastructure and retrieve the results. The backend is 
developed as a RESTful service and allows for easy extension of the framework with additional pipelines. As a usecase the \thi{prefactor} pipeline, a scientific workflow developed using both CWL and 
containerisation, was integrated into the framework.

While the current implementation fulfils the "ease-of-use" requirement, it is not yet sufficiently scalable. Currently, only a single compute infrastructure (DAS5) is available, and transferring data 
from the LTA over a regular internet connection is a significant bottleneck. In the coming months we aim to improve the scalability by making workflow processing possible on several clusters connected to 
the LTA through a high-bandwidth network, and accelerating some of the processing steps by using parallel computing.

For full calibration of LOFAR observations, the initial, direction independent calibration, using \thi{prefactor}, is followed by direction dependent calibration using \thi{killMS}. Direction independent 
calibration can be parallellized trivially by splitting the observation across subbands (sets of contiguous frequency channels) and distributing the fragments of the observation across the compute nodes 
of a cluster.  For direction dependent calibration this is not possible because the signal-to-noise ratio from additional splitting of the sky into different calibration directions (facets) would become 
too low for finding stable calibration solutions. Therefore we currently perform this on a single high memory node. However, parallellization across nodes is still possible, e.g., by summing facets from 
different subbands to increase the signal-to-noise ratio. We are currently developing this code.
%\section*{Acknowledgment}



\bibliographystyle{IEEEtran}
\bibliography{refs}

\end{document}
